package com.example.javaapp.form;

import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class MessageForm {
    @Basic
    @Column
    Long user_id;

    @Basic
    @Column
    Long topic_id;

    @Basic
    @Column
    String message;
}