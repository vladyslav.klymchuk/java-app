package com.example.javaapp.form;

import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class TopicForm {
    @Basic
    @Column
    String heading;

    @Basic
    @Column
    String info;

    @Basic
    @Column
    Long user_id;
}
