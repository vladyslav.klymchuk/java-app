package com.example.javaapp.form;

import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserForm {
    @Basic
    @Column
    String username;

    @Basic
    @Column
    String email;

    @Basic
    @Column
    String password;
}
