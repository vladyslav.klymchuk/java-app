package com.example.javaapp.controllers.api;

import com.example.javaapp.domain.Message;
import com.example.javaapp.service.MessageService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping(value = "/api/message")
public class MessageRestController {

    private final MessageService messageService;

    @GetMapping("/{topicId}")
    public List<Message> get(@PathVariable Long topicId) {
        return messageService.showMessageWithTopic(topicId);
    }
}
