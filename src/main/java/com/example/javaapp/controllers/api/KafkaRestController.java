package com.example.javaapp.controllers.api;

import com.example.javaapp.domain.Message;
import com.example.javaapp.service.MessageKafka;
import com.example.javaapp.service.CountClickService;
import com.example.javaapp.service.MessageService;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping(value = "/api/kafka")
public class KafkaRestController {
    private final CountClickService countClickService;
    private final MessageKafka messageKafka;

    private final MessageService messageService;

    @GetMapping(value = "/send")
    public String send(@RequestParam("msg") String message) {
        Message newMassage = new Message();
        newMassage.setUserId(2L);
        newMassage.setTopicId(2L);

        messageKafka.send(newMassage);
        countClickService.click();
        return "Message \"" + message + "\" sent to the Kafka successfully. " +
                "A total: " + countClickService.getCount() + " messages have been sent. ";
    }

    @GetMapping(value = "/send-msg")
    public String sendMessage(@ModelAttribute @NotNull Message message) {
        messageKafka.send(message);
        countClickService.click();
        return "Message \"" + message.getMessage() + "\" sent to the Kafka successfully. " +
                "A total: " + countClickService.getCount() + " messages have been sent. ";
    }

}
