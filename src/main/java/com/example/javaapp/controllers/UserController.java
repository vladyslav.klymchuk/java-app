package com.example.javaapp.controllers;


import com.example.javaapp.domain.User;
import com.example.javaapp.form.UserForm;
import com.example.javaapp.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
@AllArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping("/register")
    public String getRegisterPage(Model model) {
        model.addAttribute("registerRequest", new UserForm());
        return "register";
    }

    @PostMapping("/register")
    public String register(@ModelAttribute UserForm user) {
        User registerUser = userService.registerUser(user.getUsername(), user.getEmail(), user.getPassword());
        return registerUser == null ? "error" : "redirect:/login";
    }

    @GetMapping("/login")
    public String getLoginPage(Model model) {
        model.addAttribute("loginRequest", new UserForm());
        return "login";
    }

    @PostMapping("/login")
    public String login(@ModelAttribute UserForm user, Model model) {
        User authenticated = userService.authenticate(user.getEmail(), user.getPassword());
        if (authenticated != null) {
            model.addAttribute("userEmail", authenticated.getEmail());
            return "personal";
        } else {
            return "error";
        }
    }

}
