package com.example.javaapp.service;

import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicInteger;

@Service
public class CountClickService {
    private final AtomicInteger count = new AtomicInteger(0);

    public void click() {
        count.incrementAndGet();
    }
    public int getCount(){
        return count.get();
    }

}
