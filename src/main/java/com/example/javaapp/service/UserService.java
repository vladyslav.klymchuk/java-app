package com.example.javaapp.service;

import com.example.javaapp.domain.User;
import com.example.javaapp.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class UserService {
    private final UserRepository userRepository;

    public User registerUser(String username, String email, String password) {
        if (email == null || password == null) {
            return null;
        }

        if (userRepository.findFirstByEmail(email).isPresent()) {
            System.out.println("Duplicate email");
            return null;
        }

        User user = new User();
        user.setUsername(username);
        user.setEmail(email);
        user.setPassword(password);

        return userRepository.save(user);
    }

    public User authenticate(String email, String password) {
        return userRepository.findByEmailAndPassword(email, password).orElse(null);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }
}
