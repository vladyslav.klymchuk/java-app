package com.example.javaapp.service;

import com.example.javaapp.domain.Message;
import com.example.javaapp.repository.MessageRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class MessageService {

    private final MessageRepository messageRepository;

    public Message get(Long messageId) {
        return messageRepository.getReferenceById(messageId);
    }

    public Message save(Message message) {
        return messageRepository.save(message);
    }
    public List<Message> showMessageWithTopic(Long topicId) {
        return messageRepository.findMessageByTopicId(topicId);
    }
}
