package com.example.javaapp.service;

import com.example.javaapp.domain.Topic;
import com.example.javaapp.repository.TopicRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class TopicService {
    private final TopicRepository topicRepository;

    public Topic get(Long id){
        return topicRepository.getReferenceById(id);
    }
    public Topic save(Topic topic){
        return topicRepository.save(topic);
    }
}
