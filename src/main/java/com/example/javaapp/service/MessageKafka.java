package com.example.javaapp.service;

import com.example.javaapp.domain.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Service
public class MessageKafka {
    private final String TOPIC_NAME = "java_in_use_kafka";
    @Autowired
    private KafkaTemplate<String, Message> kafkaTemplate;

    public void send(Message message) {
        kafkaTemplate.send(TOPIC_NAME, message);
    }

    @KafkaListener(topics = TOPIC_NAME)
    public void listener(Message message) {
        System.out.println(message);
    }
}
