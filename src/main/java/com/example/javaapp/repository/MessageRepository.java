package com.example.javaapp.repository;

import com.example.javaapp.domain.Message;
import com.example.javaapp.domain.Topic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<Message, Long> {
//    @Query("select m from Message m WHERE m.topicId = ?1")
    List<Message> findMessageByTopicId(Long topicId);
}
