package com.example.javaapp.domain;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Table(name="topics")
public class Topic {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column
    Long id;

    @Basic
    @Column
    @NotNull(message = "heading may not be null")
    String heading;

    @Basic
    @Column
    @NotNull(message = "info may not be null")
    String info;

    @Basic
    @Column(name = "user_id")
    Long userId;
}
