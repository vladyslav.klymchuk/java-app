package com.example.javaapp.domain;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "messages")
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    Long id;

    @Basic
    @Column(name = "user_id")
    @NotNull(message = "message may not be null")
    Long userId;

    @Basic
    @Column(name = "topic_id")
    @NotNull(message = "message may not be null")
    Long topicId;

    @Basic
    @Column
    @NotNull(message = "message may not be null")
    String message;
}
