DROP TABLE IF EXISTS messages;
CREATE TABLE messages
(
    id       SERIAL       NOT NULL,
    user_id  int          NOT NULL,
    topic_id int          NOT NULL,
    message  varchar(255) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE,
    FOREIGN KEY (topic_id) REFERENCES topics (id) ON DELETE CASCADE
);