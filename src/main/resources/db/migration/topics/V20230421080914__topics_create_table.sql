DROP TABLE IF EXISTS topics;
CREATE TABLE topics
(
    id      SERIAL       NOT NULL,
    heading varchar(255) NOT NULL,
    info    varchar(255) NOT NULL,
    user_id int          NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE
);